import bs4
import base64
import csv
from datetime import datetime, timedelta
import glob
import hashlib
import os
import re
from Crypto.Cipher._mode_cbc import CbcMode
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from Crypto.Util.Padding import pad, unpad
from typing import Optional, Any, cast


def versleutel(tekst: str, wachtwoord: str) -> str:
    sleutel = base64.b64decode(wachtwoord)
    algoritme = cast(CbcMode, AES.new(sleutel, AES.MODE_CBC))
    encodedtekst = tekst.encode()
    paddedtekst = pad(encodedtekst, AES.block_size)
    versleuteld = algoritme.encrypt(paddedtekst)
    iv = base64.b64encode(algoritme.iv).decode()
    ct = base64.b64encode(versleuteld).decode()
    return iv + ct


def ontsleutel(versleuteld: str, wachtwoord: str) -> Optional[str]:
    try:
        sleutel = base64.b64decode(wachtwoord)
        iv = base64.b64decode(versleuteld[:24])
        ct = base64.b64decode(versleuteld[24:])
        algoritme = AES.new(sleutel, AES.MODE_CBC, iv=iv)
        plainbuffer = algoritme.decrypt(ct)
        unpaddedbuffer = unpad(plainbuffer, AES.block_size)
        plaintext = unpaddedbuffer.decode()
        return plaintext
    except (ValueError, KeyError):
        return None


def nieuw_wachtwoord() -> str:
    return base64.b64encode(get_random_bytes(16)).decode()


def test_versleutel_en_ontsleutel() -> None:
    wachtwoord = nieuw_wachtwoord()
    tekst = "tekst"
    assert ontsleutel(versleutel(tekst, wachtwoord), wachtwoord) == tekst


def hash_md5(tekst: str) -> str:
    return hashlib.md5(tekst.encode()).hexdigest()


def geformatteerd_bedrag(bedrag: int) -> str:
    if bedrag == 0:
        return "✕"
    teken = "＋" if bedrag > 0 else "－" if bedrag < 0 else ""
    euros = abs(bedrag) // 100
    centen = abs(bedrag) % 100
    return f"{teken}€{euros:_d},{centen:02d}".replace("_", ".")


def test_geformatteerd_bedrag() -> None:
    assert geformatteerd_bedrag(123406) == "＋€1.234,06"
    assert geformatteerd_bedrag(0) == "✕"
    assert geformatteerd_bedrag(-123400) == "－€1.234,00"


def centen(tekst: str) -> int:
    aantal_cijfers_achter_de_komma = len(tekst) - tekst.find(",") - 1 if "," in tekst else 0
    getal = int(re.sub(r"[✕＋－€,.]", "", tekst) or 0)
    return getal * pow(10, 2 - aantal_cijfers_achter_de_komma)


def test_centen() -> None:
    assert centen("1234") == 123400
    assert centen("1234,5") == 123450
    assert centen("1234,06") == 123406
    assert centen("✕") == 0
    assert centen("＋1234,06") == 123406
    assert centen("－1234,06") == 123406
    assert centen("€1234,06") == 123406
    assert centen("＋€1234,06") == 123406
    assert centen("－€1234,06") == 123406
    assert centen("1.234,06") == 123406


def melding(aantal: int, meervoud: str, werkwoord: str, voorzetsel: str, bestandsnaam: str, extra: Optional[str] = None) -> None:
    print(f"{aantal:4d} {meervoud:22s} {werkwoord:13s} {voorzetsel:4s} {bestandsnaam:21s} {f'({extra})' if extra else ''}")


def fout(tekst: str, extra: Optional[str] = None) -> None:
    print(f"FOUT {tekst} {': ' + extra if extra else ''}")
    exit(1)


def vind_transactieoverzichten(bestandsformaat_transactieoverzichten: str) -> list[str]:
    bestandsnamen_knab = glob.glob(bestandsformaat_transactieoverzichten)
    if len(bestandsnamen_knab) > 1:
        fout("meerdere transactieoverzichten gevonden", str(bestandsnamen_knab))
    return bestandsnamen_knab


def verplaats_transactieoverzichten_naar_transacties(bestandsnamen_transactieoverzichten: str, bestandsnaam_transacties: str) -> None:
    for bestandsnaam_transactieoverzicht in bestandsnamen_transactieoverzichten:
        os.replace(bestandsnaam_transactieoverzicht, bestandsnaam_transacties)
    return len(bestandsnamen_transactieoverzichten)


def vind_eerste_kolom_waar_sleutel_heeft_waarde(lijst: list, sleutel: str, waarde: str, kolom: str) -> Optional[str]:
    for element in lijst:
        if element[sleutel] == waarde:
            return element[kolom]
    return None


def bereken_saldo_per_groep(lijst: list[dict[str, str]], groep: str, kolom: str) -> dict[str, int]:
    resultaat = {}
    for element in lijst:
        if element[groep]:
            resultaat[element[groep]] = int(resultaat.get(element[groep], "0")) + int(element[kolom])
    return resultaat


def test_bereken_saldo_per_groep() -> None:
    lijst = [{"groep": "een", "kolom": "1"}, {"groep": "twee", "kolom": "1"}, {"groep": "twee", "kolom": "1"}, {"groep": "drie", "kolom": "3"}]
    assert bereken_saldo_per_groep(lijst, "groep", "kolom") == {"een": 1, "twee": 2, "drie": 3}


def lees_transacties(bestandsnaam_transacties: str) -> list[dict[str, str]]:
    transacties = []
    vorige_transactiedatum = None
    with open(bestandsnaam_transacties) as bestand_transacties:
        lezer_transacties = csv.reader(bestand_transacties, delimiter=";")
        next(lezer_transacties)
        kop_transacties = next(lezer_transacties)
        for regel_transacties in lezer_transacties:
            transactie = dict(zip(kop_transacties, regel_transacties))
            transactiedatum = datetime.strptime(transactie["Transactiedatum"], "%d-%m-%Y")
            if vorige_transactiedatum and transactiedatum > vorige_transactiedatum + timedelta(2):
                fout("transacties niet in juiste volgorde", f"{transactiedatum} > {vorige_transactiedatum}")
            vorige_transactiedatum = transactiedatum
            transactie["Centen"] = str((+1 if transactie["CreditDebet"] == "C" else -1) * centen(transactie["Bedrag"]))
            transacties.append(transactie)
    return transacties


def lees_boekingen(bestandsnaam_boekingen: str) -> list[dict[str, str]]:
    boekingen = []
    vorige_boekingsdatum = None
    with open(bestandsnaam_boekingen) as bestand_boekingen:
        lezer_boekingen = csv.reader(bestand_boekingen, delimiter=";")
        kop_boekingen = next(lezer_boekingen)
        for regel_boekingen in lezer_boekingen:
            boeking = dict(zip(kop_boekingen, regel_boekingen))
            boekingsdatum = datetime.strptime(boeking["Datum"], "%d-%m-%Y")
            if vorige_boekingsdatum and boekingsdatum < vorige_boekingsdatum:
                fout("boekingen niet in juiste volgorde", f"{boekingsdatum} < {vorige_boekingsdatum}")
            vorige_boekingsdatum = boekingsdatum
            boeking["Referentie"] = boeking["Referentie"].replace("#", "")
            boeking["Boekingsnummer"] = boeking["Boekingsnummer"].replace("#", "")
            boeking["Jaar"] = str(boekingsdatum.year)
            boeking["Centen"] = str(centen(boeking["Bedrag"]))
            boekingen.insert(0, boeking)
    return boekingen


def vind_projecten(boekingen: list[dict[str, str]]) -> set[str]:
    return set([boeking["Project"] for boeking in boekingen])


def vind_jaren(boekingen: list[dict[str, str]]) -> set[str]:
    return set([boeking["Jaar"] for boeking in boekingen])


def bereken_totalen(boekingen: list[dict[str, str]], jaren: set[str], projecten: set[str]) -> None:
    titels_totalen = projecten | {"Alle transacties", "Alleen banktransacties"}
    basis_totalen = {"Begin": 0, "Ontvangen": 0, "Uitgegeven": 0, "Einde": 0}
    vorige_jaren = jaren - {str(datetime.now().year)}
    totalen = {project: {jaar: dict(basis_totalen) for jaar in vorige_jaren} for project in titels_totalen}
    for boeking in boekingen:
        if boeking["Jaar"] in vorige_jaren:
            centen = int(boeking["Centen"])
            uitgegeven = centen if centen < 0 else 0
            ontvangen = centen if centen > 0 else 0
            bij_te_werken_projecten = {boeking["Project"], "Alle transacties", "Alleen banktransacties"} if boeking["Referentie"] else {boeking["Project"], "Alle transacties"}
            for project in bij_te_werken_projecten:
                project_totaal = totalen[project]
                for jaar in vorige_jaren:
                    jaar_totaal = project_totaal[jaar]
                    if jaar == boeking["Jaar"]:
                        jaar_totaal["Ontvangen"] += ontvangen
                        jaar_totaal["Uitgegeven"] += uitgegeven
                        jaar_totaal["Einde"] += ontvangen + uitgegeven
                    elif jaar > boeking["Jaar"]:
                        jaar_totaal["Begin"] += ontvangen + uitgegeven
                        jaar_totaal["Einde"] += ontvangen + uitgegeven
    return totalen


def lees_totalen(bestandsnaam_totalen: str, matrix: list[list[str]]) -> None:
    with open(bestandsnaam_totalen, "r", newline="") as bestand_totalen:
        csv_bestand = csv.reader(bestand_totalen, dialect="unix")
        for waardes in matrix:
            gelezen_waardes = csv_bestand.__next__()
            if waardes != gelezen_waardes:
                fout("gelezen totaal anders dan verwacht", f"{gelezen_waardes} != {waardes}")


def schrijf_totalen(bestandsnaam_totalen: str, matrix: list[list[str]]) -> None:
    with open(bestandsnaam_totalen, "w", newline="") as bestand_totalen:
        csv_bestand = csv.writer(bestand_totalen, dialect="unix")
        csv_bestand.writerows(matrix)


def controleer_totalen(bestandsnaam_totalen: str, totalen: dict[str, dict[str, dict[str, int]]]) -> None:
    matrix = []
    gesorteerde_projecten = sorted(totalen.keys(), key=lambda project: "!" + project if project in ["Alle transacties", "Alleen banktransacties"] else project)
    gesorteerde_jaren = sorted(totalen["Alle transacties"].keys())
    matrix.append(["Project", "Jaar", "Begin", "Ontvangen", "Uitgegeven", "Einde"])
    for project in gesorteerde_projecten:
        project_totaal = totalen[project]
        for jaar in gesorteerde_jaren:
            jaar_totaal = project_totaal[jaar]
            matrix.append([project, jaar, str(jaar_totaal["Begin"]), str(jaar_totaal["Ontvangen"]), str(jaar_totaal["Uitgegeven"]), str(jaar_totaal["Einde"])])
    if os.path.isfile(bestandsnaam_totalen):
        lees_totalen(bestandsnaam_totalen, matrix)
    else:
        schrijf_totalen(bestandsnaam_totalen, matrix)
    return len(matrix) - 1


def controleer_bedragen(transacties: list[dict[str, str]], boekingen: list[dict[str, str]]) -> None:
    bedragen_transacties = bereken_saldo_per_groep(transacties, "Referentie", "Centen")
    bedragen_boekingen = bereken_saldo_per_groep(boekingen, "Referentie", "Centen")
    resterende_bedragen = set(bedragen_transacties.items()) ^ set(bedragen_boekingen.items())
    if resterende_bedragen:
        fout("boekingen en transacties niet in evenwicht", str(resterende_bedragen))
    return bedragen_transacties


def lees_index(bestandsnaam_index: str) -> list[str]:
    with open(bestandsnaam_index) as bestand_index:
        regels = bestand_index.readlines()
    return regels


def construeer_dom(regels: list[str]) -> bs4.BeautifulSoup:
    return bs4.BeautifulSoup("".join(regels), "html.parser")


def schrijf_index(bestandsnaam_index: str, regels: list[str]) -> None:
    with open(bestandsnaam_index, "w") as bestand_index:
        bestand_index.writelines(regels)


def lees_wachtwoorden(bestandsnaam_wachtwoorden: str) -> dict[str, str]:
    wachtwoorden = {}
    with open(bestandsnaam_wachtwoorden) as bestand_wachtwoorden:
        lezer_wachtwoorden = csv.reader(bestand_wachtwoorden, delimiter=";")
        kop = next(lezer_wachtwoorden)
        for regel in lezer_wachtwoorden:
            combinatie = dict(zip(kop, regel))
            wachtwoorden[combinatie["Project"]] = combinatie["Wachtwoord"]
    return wachtwoorden


def voegtoe_wachtwoorden(wachtwoorden: dict[str, str], boekingen: list[dict[str, str]], projecten: set[str]) -> dict[str, str]:
    nieuwe_wachtwoorden = {}
    for project in projecten:
        if project not in wachtwoorden:
            nieuwe_wachtwoorden[project] = nieuw_wachtwoord()
    return nieuwe_wachtwoorden


def schrijf_wachtwoorden(bestandsnaam_wachtwoorden: str, wachtwoorden: dict[str, str]) -> None:
    with open(bestandsnaam_wachtwoorden, "w", newline="") as bestand_wachtwoorden:
        schrijver_wachtwoorden = csv.writer(bestand_wachtwoorden, delimiter=";", quotechar='"', quoting=csv.QUOTE_ALL)
        schrijver_wachtwoorden.writerow(["Wachtwoord", "Project"])
        for project, wachtwoord in wachtwoorden.items():
            schrijver_wachtwoorden.writerow([wachtwoord, project])
    return wachtwoorden


def vind_tabel_boekingen(regels: list[str]) -> Any:
    tabel = construeer_dom(regels).find("table", class_="boekingen")
    return tabel.find("tbody") if tabel else None


def verwijder_boekingen(regels: list[str]) -> dict[str, str]:
    oude_boekingen = vind_tabel_boekingen(regels).find_all("tr")
    hashes = {}
    for oude_boeking in reversed(oude_boekingen):
        regel = regels.pop(oude_boeking.sourceline - 1)
        match = re.search(r"data-hash=\"(\w+)\"", regel)
        hash = match.group(1) if match else ""
        hashes[hash] = regel
    return regels, hashes


def voegtoe_boekingen(regels: list[str], boekingen: list[dict[str, str]], hashes: dict[str, str], transacties: list[dict[str, str]], wachtwoorden: dict[str, str]) -> list[str]:
    tabel_boekingen = vind_tabel_boekingen(regels)
    for boeking in reversed(boekingen):
        hash = hash_md5(str(boeking))
        if hash in hashes:
            rij_html = hashes[hash]
        else:
            bedrag_in_centen = int(boeking["Centen"])
            bedrag = geformatteerd_bedrag(bedrag_in_centen)
            ontvangen = bedrag if bedrag_in_centen > 0 else ""
            uitgegeven = bedrag if bedrag_in_centen < 0 else ""
            referentie = boeking["Referentie"]
            onversleutelde_html = (
                f"<td>{vind_eerste_kolom_waar_sleutel_heeft_waarde(transacties, 'Referentie', referentie, 'Tegenrekeningnummer')}</td>"
                f"<td>{vind_eerste_kolom_waar_sleutel_heeft_waarde(transacties, 'Referentie', referentie, 'Tegenrekeninghouder')}</td>"
                f"<td>{vind_eerste_kolom_waar_sleutel_heeft_waarde(transacties, 'Referentie', referentie, 'Omschrijving')}</td>"
            )
            versleutelde_html = versleutel(onversleutelde_html, wachtwoorden[boeking["Project"]])
            rij_html = (
                f"{'':<16s}"
                f"<tr data-hash=\"{hash}\" data-versleuteld=\"{versleutelde_html}\">"
                f"<td>{boeking['Datum']}</td>"
                f"<td>{boeking['Project']}</td>"
                f"<td>{ontvangen}</td>"
                f"<td>{uitgegeven}</td>"
                f"<td>{boeking['Referentie']}</td>"
                f"<td>{boeking['Boekingsnummer']}</td>"
                f"</tr>\n"
            )
        regels.insert(tabel_boekingen.sourceline, rij_html)
    return regels


def test():
    test_versleutel_en_ontsleutel()
    test_geformatteerd_bedrag()
    test_centen()
    test_bereken_saldo_per_groep()


def main():
    BESTANDSFORMAAT_TRANSACTIEOVERZICHTEN = "Knab Transactieoverzicht * NL62KNAB0721682960 - *.csv"
    BESTANDSNAAM_BOEKINGEN = "boekingen.csv"
    BESTANDSNAAM_INDEX = "index.html"
    BESTANDSNAAM_TOTALEN = "totalen.csv"
    BESTANDSNAAM_TRANSACTIES = "transacties.csv"
    BESTANDSNAAM_WACHTWOORDEN = "wachtwoorden.csv"

    transactieoverzichten = vind_transactieoverzichten(BESTANDSFORMAAT_TRANSACTIEOVERZICHTEN)
    melding(len(transactieoverzichten), "transactieoverzichten", "gevonden", "in", ".")

    aantal_verplaatst = verplaats_transactieoverzichten_naar_transacties(transactieoverzichten, BESTANDSNAAM_TRANSACTIES)
    melding(aantal_verplaatst, "transactieoverzichten", "hernoemd", "in", BESTANDSNAAM_TRANSACTIES)

    boekingen = lees_boekingen(BESTANDSNAAM_BOEKINGEN)
    melding(len(boekingen), "boekingen", "gelezen", "uit", BESTANDSNAAM_BOEKINGEN)

    projecten = vind_projecten(boekingen)
    melding(len(projecten), "projecten", "gevonden", "in", "boekingen")

    jaren = vind_jaren(boekingen)
    melding(len(jaren), "jaren", "gevonden", "in", "boekingen")

    totalen = bereken_totalen(boekingen, jaren, projecten)
    melding(sum([len(totalen[project]) for project in totalen]), "totalen", "berekend", "uit", "boekingen")

    aantal_totalen = controleer_totalen(BESTANDSNAAM_TOTALEN, totalen)
    melding(aantal_totalen, "totalen", "gecontroleerd", "in", BESTANDSNAAM_TOTALEN)

    transacties = lees_transacties(BESTANDSNAAM_TRANSACTIES)
    melding(len(transacties), "transacties", "gelezen", "uit", BESTANDSNAAM_TRANSACTIES)

    bedragen = controleer_bedragen(transacties, boekingen)
    melding(len(transacties), "transacties", "verantwoord", "in", "boekingen")

    oude_wachtwoorden = lees_wachtwoorden(BESTANDSNAAM_WACHTWOORDEN)
    melding(len(oude_wachtwoorden), "wachtwoorden", "gelezen", "uit", BESTANDSNAAM_WACHTWOORDEN)

    nieuwe_wachtwoorden = voegtoe_wachtwoorden(oude_wachtwoorden, boekingen, projecten)
    melding(len(nieuwe_wachtwoorden), "wachtwoorden", "toegevoegd", "aan", "wachtwoorden")

    alle_wachtwoorden = schrijf_wachtwoorden(BESTANDSNAAM_WACHTWOORDEN, oude_wachtwoorden | nieuwe_wachtwoorden)
    melding(len(alle_wachtwoorden), "wachtwoorden", "geschreven", "in", BESTANDSNAAM_WACHTWOORDEN)

    regels = lees_index(BESTANDSNAAM_INDEX)
    melding(len(regels), "regels", "gelezen", "uit", BESTANDSNAAM_INDEX)

    regels, hashes = verwijder_boekingen(regels)
    melding(len(hashes), "boekingen", "verwijderd", "uit", "index")

    regels = voegtoe_boekingen(regels, boekingen, hashes, transacties, alle_wachtwoorden)
    melding(len(boekingen), "boekingen", "toegevoegd", "aan", "index")

    schrijf_index(BESTANDSNAAM_INDEX, regels)
    melding(len(regels), "regels", "geschreven", "in", BESTANDSNAAM_INDEX)


if __name__ == "__main__":
    test()
    main()
