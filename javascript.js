window.addEventListener("load",
  function() {
    const ALLE_TRANSACTIES = "Alle transacties"
    const ALLEEN_BANKTRANSACTIES = "Alleen banktransacties"
    const ALLE_JAREN = "Alle jaren"
    const ALLE_KOLOMMEN = ["begin", "ontvangen", "uitgegeven", "einde"]


    const newElement = function(tagName, attributes, content) {
      const element = document.createElement(tagName)
      Object.keys(attributes).forEach(attribute => element.setAttribute(attribute, attributes[attribute]))
      const children = Array.isArray(content) ? content : [content]
      const nodes = children.map(child => child instanceof Node ? child : document.createTextNode(child))
      nodes.forEach(node => element.appendChild(node))
      return element
    }

    const geformatteerd_bedrag = function(bedrag, met_plusteken) {
      if (!bedrag)
        return "✕"
      const positief_bedrag_in_centen = Math.abs(bedrag)
      const centen = positief_bedrag_in_centen % 100
      const euros = Math.round((positief_bedrag_in_centen - centen) / 100)
      const tekst_teken = bedrag < 0 ? "－" : (met_plusteken && bedrag > 0 ? "＋" : "")
      const tekst_euros = String(euros).replace(/(\d)(?=(\d\d\d)+$)/g, "$1.")
      const tekst_centen = String(centen).replace(/^(\d)$/, "0$1")
      return `${tekst_teken}€${tekst_euros},${tekst_centen}`
    }

    const kind = function(rij, positie) {
      return rij.children[positie].textContent
    }

    const centen = function(tekst) {
      return Number(tekst.replace(/[✕＋－€,.]/g, ""))
    }

    const centen_in = function(boeking, positie) {
      return centen(kind(boeking, positie))
    }

    const jaar_in = function(boeking, positie=0) {
      return Number(kind(boeking, positie).split("-")[2])
    }

    const project_in = function(boeking, positie=1) {
      return kind(boeking, positie).trim()
    }

    const referentie_in = function(boeking, positie=4) {
      return kind(boeking, positie).trim()
    }

    const toon_kolommen = function(tabel_boekingen, posities, tonen) {
      const rijen = Array.from(tabel_boekingen.querySelectorAll("tr"))
      rijen.forEach(rij => {
        posities.forEach(positie => {
          if (rij.children.length > positie)
            rij.children[positie].style.display = tonen ? "table-cell" : "none"
        })
      })
    }

    function b64decode(tekst) {
      return new Uint8Array(atob(tekst).split("").map(karakter => karakter.charCodeAt(0)))
    }

    async function ontsleutel(versleuteld, wachtwoord) {
      const sleutel = b64decode(wachtwoord)
      const iv = b64decode(versleuteld.slice(0, 24))
      const ct = b64decode(versleuteld.slice(24))
      const algoritme = { name: "AES-CBC", iv: iv }
      const key = await crypto.subtle.importKey("raw", sleutel, algoritme, false, ["decrypt"])
      const plainbuffer = await crypto.subtle.decrypt(algoritme, key, ct)
      const plaintext = new TextDecoder().decode(plainbuffer)
      return plaintext
    }

    const tabel_boekingen = document.querySelector(".boekingen")
    const boekingen = Array.from(tabel_boekingen.querySelectorAll("tbody tr"))
    const jaren = Array.from(new Set(boekingen.map(boeking => jaar_in(boeking)))).sort().reverse()
    const projecten = Array.from(new Set(boekingen.map(boeking => project_in(boeking)))).sort()

    document.querySelector(".totaalaantal-projecten").textContent = projecten.length

    document.querySelector("#wachtwoord").addEventListener("change", event => {
      const element = document.querySelector("#wachtwoord")
      const wachtwoord = element.value
      element.value = ""

      boekingen.forEach(boeking => {
        if (boeking.style.display == "table-row") {
          const versleutelde_html = boeking.dataset["versleuteld"]
          ontsleutel(versleutelde_html, wachtwoord).then(function(ontsleutelde_html) {
            boeking.insertAdjacentHTML("beforeend", ontsleutelde_html)
          }).catch(function() { })
        }
      })

      setTimeout(function() {
        if (boekingen.some(boeking => boeking.children.length > 5))
          toon_kolommen(tabel_boekingen, [5, 6, 7], true)
        else
          alert("Fout wachtwoord")
      }, 500)
    })

    Array.from(document.querySelectorAll(".filter-project")).forEach(paragraaf => {
      paragraaf.prepend(
        newElement("select", { class: "invoer-project", name: "project" },
          [newElement("option", {}, ALLEEN_BANKTRANSACTIES),
          newElement("option", {}, ALLE_TRANSACTIES)].concat(
            projecten.map(project => newElement("option", { "value": project }, "Alleen project " + project))
          )
        )
      )
    })

    Array.from(document.querySelectorAll(".filter-jaar")).forEach(paragraaf => {
      paragraaf.prepend(
        newElement("select", { class: "invoer-jaar", name: "jaar" },
          [newElement("option", { "value": 0 }, ALLE_JAREN)].concat(
            jaren.map(jaar => newElement("option", { "value": jaar }, "Alleen " + jaar))
          )
        )
      )
    })

    document.querySelectorAll(".filter").forEach(filter => {
      filter.style.display = "block"
    })

    const basis_totalen = [["begin", 0], ["ontvangen", 0], ["uitgegeven", 0], ["einde", 0]]
    const project_jaar_totalen = new Map(
      [ALLE_TRANSACTIES, ALLEEN_BANKTRANSACTIES].concat(projecten).map(
        project => [project, new Map(basis_totalen.concat(jaren.map(jaar => [jaar, new Map(basis_totalen)])))]
      )
    )
    boekingen.forEach(boeking => {
      const huidige_jaar = jaar_in(boeking)
      const huidige_project = project_in(boeking)
      const huidige_referentie = referentie_in(boeking)
      const ontvangen = centen_in(boeking, 2)
      const uitgegeven = centen_in(boeking, 3)

      const bij_te_werken_projecten = [huidige_project, ALLE_TRANSACTIES]
      if (huidige_referentie)
        bij_te_werken_projecten.push(ALLEEN_BANKTRANSACTIES)

      bij_te_werken_projecten.forEach(project => {
        const project_totaal = project_jaar_totalen.get(project)
        project_totaal.set("ontvangen", project_totaal.get("ontvangen") + ontvangen)
        project_totaal.set("uitgegeven", project_totaal.get("uitgegeven") - uitgegeven)
        project_totaal.set("einde", project_totaal.get("einde") + ontvangen - uitgegeven)
        jaren.forEach(jaar => {
          const jaar_totaal = project_totaal.get(jaar)
          if (jaar == huidige_jaar) {
            jaar_totaal.set("ontvangen", jaar_totaal.get("ontvangen") + ontvangen)
            jaar_totaal.set("uitgegeven", jaar_totaal.get("uitgegeven") - uitgegeven)
            jaar_totaal.set("einde", jaar_totaal.get("einde") + ontvangen - uitgegeven)
          }
          else if (jaar > huidige_jaar) {
            jaar_totaal.set("begin", jaar_totaal.get("begin") + ontvangen - uitgegeven)
            jaar_totaal.set("einde", jaar_totaal.get("einde") + ontvangen - uitgegeven)
          }
        })
      })
    })

    toon_kolommen(tabel_boekingen, [6, 7, 8], false)

    const vul_rij = function(rij, totaal, kolommen) {
      ALLE_KOLOMMEN.forEach((kolom, index) => {
        if (kolommen.includes(kolom)) {
          const element = rij.children[index + 1]
          const bedrag = totaal.get(kolom)
          const met_plusteken = kolom == "ontvangen"
          element.textContent = geformatteerd_bedrag(bedrag, met_plusteken)
        }
      })
      return totaal.get("ontvangen") || totaal.get("uitgegeven")
    }

    const filter_boekingen = function(project, jaar) {
      const is_project_gekozen = project != ALLE_TRANSACTIES && project != ALLEEN_BANKTRANSACTIES
      const is_jaar_gekozen = jaar != 0
      toon_kolommen(tabel_boekingen, [1], !is_project_gekozen)

      const totaal_aantal_boekingen = boekingen.length
      const aantal_getoonde_boekingen = boekingen.filter(boeking => {
        const huidige_jaar = jaar_in(boeking)
        const huidige_project = project_in(boeking)
        const huidige_referentie = referentie_in(boeking)
        const is_juiste_project = project == ALLE_TRANSACTIES ||
          (project == ALLEEN_BANKTRANSACTIES && huidige_referentie) ||
          huidige_project == project
        const is_juiste_jaar = !is_jaar_gekozen || huidige_jaar == jaar
        const is_getoond = is_juiste_project && is_juiste_jaar
        boeking.style.display = is_getoond ? "table-row" : "none"
        return is_getoond
      }).length
      const aantal_tekst = `${aantal_getoonde_boekingen} van de ${totaal_aantal_boekingen} transacties`
      const project_tekst = is_project_gekozen ? 'Alleen project ' + project : project
      const jaar_tekst = is_jaar_gekozen ? 'Alleen ' + jaar : ALLE_JAREN
      const conditie_tekst = `voor "${project_tekst}" en "${jaar_tekst}"`
      const paragraaf_aantal = document.querySelector(".aantal-boekingen")
      paragraaf_aantal.textContent = `${aantal_tekst} ${conditie_tekst}.`

      const project_totaal = project_jaar_totalen.get(project)
      const tabel_totalen = document.querySelector(".totalen")
      Array.from(tabel_totalen.querySelectorAll("tbody tr")).forEach(totaalrij => {
        const jaar_rij = Number(totaalrij.children[0].textContent)
        const jaar_totaal = project_totaal.get(jaar_rij)
        vul_rij(totaalrij, jaar_totaal, ALLE_KOLOMMEN)
      })
      vul_rij(tabel_totalen.querySelector("tfoot tr"), project_totaal, ALLE_KOLOMMEN)

      const project_of_jaar_totaal = is_jaar_gekozen ? project_totaal.get(jaar) : project_totaal
      const voetrij = tabel_boekingen.querySelector("tfoot tr")
      const is_met_boekingen = vul_rij(voetrij, project_of_jaar_totaal, ["ontvangen", "uitgegeven"])
      tabel_boekingen.style.display = is_met_boekingen ? "table" : "none"
      const element_wachtwoord = document.querySelector(".wachtwoord")
      element_wachtwoord.style.display = is_project_gekozen && is_met_boekingen ? "block" : "none"
    }

    const filter_projecten = function(project, jaar) {
      const is_jaar_gekozen = jaar != 0
      const tabel_projecten = document.querySelector(".projecten")
      const projecten = Array.from(tabel_projecten.querySelectorAll("tbody tr"))

      const totaal_aantal_projecten = projecten.length
      const aantal_getoonde_projecten = projecten.filter(projectrij => {
        const project = projectrij.children[0].textContent
        const project_totaal = project_jaar_totalen.get(project)
        const project_of_jaar_totaal = is_jaar_gekozen ? project_totaal.get(jaar) : project_totaal
        vul_rij(projectrij, project_of_jaar_totaal, ALLE_KOLOMMEN)
        const is_getoond = ALLE_KOLOMMEN.some(kolom => project_of_jaar_totaal.get(kolom))
        projectrij.style.display = is_getoond ? "table-row" : "none"
        return is_getoond
      }).length
      const aantal_tekst = `${aantal_getoonde_projecten} van de ${totaal_aantal_projecten} projecten`
      const jaar_tekst = is_jaar_gekozen ? 'Alleen ' + jaar : ALLE_JAREN
      const conditie_tekst = `voor "${jaar_tekst}"`
      const paragraaf_aantal = document.querySelector(".aantal-projecten")
      paragraaf_aantal.textContent = `${aantal_tekst} ${conditie_tekst}.`

      const transacties_totaal = project_jaar_totalen.get(ALLE_TRANSACTIES)
      const transacties_of_jaar_totaal = is_jaar_gekozen ? transacties_totaal.get(jaar) : transacties_totaal
      vul_rij(tabel_projecten.querySelector("tfoot tr"), transacties_of_jaar_totaal, ALLE_KOLOMMEN)
    }

    const invoeren_project = Array.from(document.querySelectorAll(".invoer-project"))
    const invoeren_jaar = Array.from(document.querySelectorAll(".invoer-jaar"))

    const verwerk_wijziging = function(nieuw_project, nieuw_jaar) {
      const project = nieuw_project || invoeren_project[0].value
      const jaar = Number(nieuw_jaar || invoeren_jaar[0].value)
      invoeren_project.forEach(invoer_project => { invoer_project.value = project })
      invoeren_jaar.forEach(invoer_jaar => { invoer_jaar.value = jaar })
      filter_boekingen(project, jaar)
      filter_projecten(project, jaar)
    }

    const vervang_adres = function(verander) {
      const adres = new URL(window.location.href)
      verander(adres)
      window.history.replaceState({}, '', adres.toString())
    }

    invoeren_project.forEach(invoer_project => {
      invoer_project.addEventListener("change", function(event) {
        verwerk_wijziging(event.target.value, undefined)
        if (event.target.value == ALLEEN_BANKTRANSACTIES)
          vervang_adres(adres => adres.searchParams.delete("invoer-project"))
        else
          vervang_adres(adres => adres.searchParams.set("invoer-project", event.target.value))
      })
    })

    invoeren_jaar.forEach(invoer_jaar => {
      invoer_jaar.addEventListener("change", function(event) {
        verwerk_wijziging(undefined, event.target.value)
        if (event.target.value == 0)
          vervang_adres(adres => adres.searchParams.delete("invoer-jaar"))
        else
          vervang_adres(adres => adres.searchParams.set("invoer-jaar", event.target.value))
      })
    })

    const details = document.querySelectorAll("details")
    details.forEach(detail => { 
      detail.addEventListener("toggle", function(event) {
        if (event.target.hasAttribute("open"))
          vervang_adres(adres => adres.hash = event.target.className)
        else
          vervang_adres(adres => adres.hash = "")
      })
    })

    verwerk_wijziging(undefined, undefined)

    const open_details = function() {
      const hash = window.location.hash
      if (hash) {
        const klasse = `.${hash.substring(1)}`
        const details = Array.from(document.querySelectorAll(klasse))
        details.forEach(detail => detail.open = true)
        details[0].scrollIntoView()
      }
    }

    const vul_vragen = function() {
      const query_string = window.location.search
      const parameters = new URLSearchParams(query_string)
      const standaardwaardes = new Map([
        ["invoer-project", ALLEEN_BANKTRANSACTIES],
        ["invoer-jaar", new Date().getFullYear()]
      ])
      standaardwaardes.forEach((standaardwaarde, invoernaam) => {
        const waarde = parameters.get(invoernaam) || standaardwaarde
        const klasse = `.${invoernaam}`
        const invoeren = Array.from(document.querySelectorAll(klasse))
        invoeren.forEach(invoer => {
          invoer.value = waarde
          invoer.dispatchEvent(new Event("change"))
        })
      })
    }

    open_details()
    vul_vragen()
  }
)
